
-- SUMMARY --

This module allows you to easily and quickly integrate your Ubercart online-shop
with Pangora, a Germany based service to list your products automatically on
many different websites.

It currently comes with the following features:

1) Provide an automatically updated list of all your products in the required
XML-format for Pangora to be downloaded by Pangora on a regular basis to integrate
the given information and detail into their partner's listings. The URL of that
XML-file/product-list is http://www.pangora.com/products.pangora.xml

2) It implements the Pangora Sales Tracking functionality by prodividing some detail
to Pangora whenever an order status gets changed to 'completed'.

More details about Pangora can be found on Pangora's website (http://www.pangora.com).

-- REQUIREMENTS --

* Drupal 6 and Ubercart module 2.0.

* The Token module.

* The Token_Node module.

-- INSTALLATION --

* Install as usual, see http://drupal.org/node/70151 for further information.

-- CONFIGURATION --

* Configure Pangora in Administer >> Store Administration >> Pangora

-- CUSTOMIZATION --

None

-- TROUBLESHOOTING --

Not known yet

-- FAQ --

Yet to come

-- CONTACT --

Current maintainer:
* J�rgen Haas (jurgenhaas) - http://drupal.org/user/168924

This project has been sponsored by:
* PARAGON Executive Services GmbH
  Providing IT services as individual as the requirements. Find out more
  from http://www.paragon-es.de

