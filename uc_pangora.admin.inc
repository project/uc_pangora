<?php

/**
 * @file
 * Forms to configure Pangora for Drupal.
 */

/**
 *
 */
function uc_pangora_settings() {
  $form['uc_pangora_active'] = array(
    '#type' => 'checkbox',
    '#title' => t('Activate Pangora.'),
    '#default_value' => uc_pangora_var('active'),
  );
  $form['uc_pangora_id'] = array(
    '#type' => 'textfield',
    '#title' => t('Your Pangora ID'),
    '#size' => 50,
    '#maxlength' => 50,
    '#default_value' => uc_pangora_var('id'),
  );
  $vocabularies = array();
  $result = db_query("SELECT * FROM {vocabulary}");
  while ($voc = db_fetch_object($result)) {
    $vocabularies[$voc->vid] = $voc->name;
  }
  $form['uc_pangora_vid_category'] = array(
    '#type' => 'select',
    '#title' => t('Vocabulary for the Category'),
    '#options' => $vocabularies,
    '#default_value' => uc_pangora_var('vid_category'),
  );
  $form['uc_pangora_vid_manufacturer'] = array(
    '#type' => 'select',
    '#title' => t('Vocabulary for the Manufacturer'),
    '#options' => $vocabularies,
    '#default_value' => uc_pangora_var('vid_manufacturer'),
  );
  $form['uc_pangora_vid_brand'] = array(
    '#type' => 'select',
    '#title' => t('Vocabulary for the Brand'),
    '#options' => $vocabularies,
    '#default_value' => uc_pangora_var('vid_brand'),
  );
  $form['uc_pangora_pattern_description'] = array(
    '#type' => 'textarea',
    '#title' => t('Text for product description'),
    '#size' => 50,
    '#maxlength' => 5000,
    '#default_value' => uc_pangora_var('pattern_description'),
  );
  return system_settings_form($form);
}
